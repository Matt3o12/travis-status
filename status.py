from __future__ import print_function
import requests

API_URL = "https://api.travis-ci.org/repos/{}/branches"

TERM_GREEN = '\033[92m'
TERM_YELLOW = '\033[93m'
TERM_RED = '\033[91m'
TERM_RESET = '\033[0m'
TERM_BOLD = '\033[1m'

color_mapping = {
    "passed": TERM_GREEN,
    "failed": TERM_RED,
    "errored": TERM_RED,
    "created": TERM_YELLOW,
    "started": TERM_YELLOW,
}


def load_page(repo):
    headers = {"ACCEPT": "application/vnd.travis-ci.2+json"}
    page = requests.get(API_URL.format(repo), headers=headers)
    page.raise_for_status()
    return page.json()


def find_status(page, commit_it):
    for branch in page["branches"]:
        if branch["commit_id"] == commit_it:
            return branch["state"]

    return "unknown"


def main(args):
    if len(args) != 1:
        raise Exception("Please call with one repo name.")

    results = load_page(args[0])
    length = 0
    found_branches = []
    for branch in results["commits"]:
        branch_name = branch["branch"]
        status = find_status(results, branch["id"])

        if len(branch_name) > length:
            length = len(branch_name)

        found_branches.append((branch_name, status))

    print("Repo: {}{}{}".format(TERM_BOLD, args[0], TERM_RESET))
    for name, status in found_branches:
        color = color_mapping.get(status, "")
        print("{:<{padding}}: {}{}{}".format(name,
                                             color,
                                             status,
                                             TERM_RESET,
                                             padding=length + 4))


if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
