# Travis Status #

Travis shows the status of the most recents branches for your [Travis-CI](https://travis-ci.org/) project.

## Running ##

To get the latest status, simply run:

	python status.py Matt3o12/TextRegonizer
	
Travis Status requires requests and python2.7 or python3.3+.

## Miscellaneous ##

### Run endlessly in fish ###

	while true; while timeout 5 python PATH_TO_SCRIPT.PY Author/Project; end; sleep 15; end
	
This script only works in [Fish](fishshell.com). It will run status.py endlessly so that the news statuses are always fetched. If the script times out (i.e. it takes longer then 5 seconds), it will automatically restart the script. If `python script.py` returned an error, it will wait for 15 seconds and then retry. Please add a sleep if your ping to Travis-CI is too high so that you won't hit a rate-limit.   
*Note:* It is gtimeout on MacOS X.